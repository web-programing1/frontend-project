import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: {
        defaults: HomeView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: {
        defaults: () => import("../views/AboutView.vue"),
        menu: () => import("@/components/menus/AboutMenu.vue"),
        header: () => import("@/components/header/AboutHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: "/product",
      name: "product",

      component: {
        defaults: () => import("../views/products/ProductView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: "/product/full",
      name: "product fullscreen",

      component: {
        defaults: () => import("../views/products/ProductView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue")
      },
      meta: {
        layout: 'FullLayout',
      }
    },
  ],
});

export default router;
