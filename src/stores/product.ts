import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/sevices/product"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const products = ref<Product[]>([])
  const editedProduct = ref<Product>({name: "", price: 0});
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      console.log(res);
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้" + error)
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if(editedProduct.value.id){
        const res = await productService.updateProduct(editedProduct.value.id, editedProduct.value);
      }else{
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      clearProduct();
      await getProducts();
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้" + error)
    }
    loadingStore.isLoading = false;
  }

  function clearProduct() {
    editedProduct.value = {name: "", price: 0};
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      if(editedProduct.value.id){
        const res = await productService.deleteProduct(editedProduct.value.id)
        await getProducts();
      }
    } catch (error) {
      console.log(error)
      messageStore.showError("ไม่สามารถลบ Product ได้" + error)
    }
    loadingStore.isLoading = false;
  }

  return { products, getProducts, dialog, editedProduct, saveProduct, clearProduct, editProduct, deleteProduct};  
});
